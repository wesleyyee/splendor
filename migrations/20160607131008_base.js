
var users = function(table) {
      table.increments('id').primary()
      table.string('username')
      table.string('password')
    },
    games = function(table) {
      table.uuid('id').primary()
    };

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('users', users)
        .then(function () {
          console.log('users table is created!');
        }),
    knex.schema.createTable('games', games)
        .then(function () {
          console.log('games table is created!');
        })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('users', users)
        .then(function () {
          console.log('users table is dropped!');
        }),
    knex.schema.dropTable('games', games)
        .then(function () {
          console.log('games table is dropped!');
        })
  ])
};
