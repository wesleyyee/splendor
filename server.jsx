import koa                       from 'koa';
import serve                     from 'koa-static';
import bodyParser                from 'koa-bodyparser';
import co                        from 'co';
import React                     from 'react';
import http                      from 'http';
import request                   from 'koa-request';
import jayson                    from 'jayson';
import jwt                       from 'jsonwebtoken';
import { Promise }               from 'bluebird';
import { renderToString }        from 'react-dom/server';
import { RoutingContext, match } from 'react-router';
import createLocation            from 'history/lib/createLocation';
import routes                    from 'routes';
import { Provider }              from 'react-redux';
import { auth, game, lobby }     from 'reducers';
import thunkMiddleware           from 'redux-thunk'
import { default as api }        from 'middleware/api'
import fetchComponentData        from 'lib/fetchComponentData';
import { createStore,
         combineReducers,
         applyMiddleware }       from 'redux';
import path                      from 'path';
import router                    from './router'
import { config }                from './config.js'
import socketio                  from 'socket.io'

const app = koa();

if (process.env.NODE_ENV !== 'production') {
  require('./webpack.dev').default(app);
}

app.use(bodyParser());
app.use(serve(path.join(__dirname, 'dist')));
app.use(function* (next) {
  console.log("header", this.req.headers.authorization)
  var token = jwt.decode(this.req.headers.authorization, config.secret);
  console.log("token", token)
  yield next
})
app.use(router.middleware());

/**
 * VIEW ROUTES
 */
app.use( function* () {

  const reducer = combineReducers({
    auth,
    game,
    lobby
  });
  const location = createLocation(this.req.url);
  const store    = applyMiddleware(thunkMiddleware, api)(createStore)(reducer);

  match({ routes, location }, (err, redirectLocation, renderProps) => {
    if(err) {
      console.error(err);
      this.status = 500;
      this.body = 'Internal server error';
      return
    }

    if(!renderProps) {
      this.status = 404;
      this.body = 'Not Found';
      return
    }

    function renderView() {
      const InitialView = (
        <Provider store={store}>
          <RoutingContext {...renderProps} />
        </Provider>
      );

      const componentHTML = renderToString(InitialView);

      const initialState = store.getState();

      const HTML = `
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="utf-8">
          <title>Splendor</title>
          <link rel="stylesheet" href="/styles.css">
          <script>
            window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
          </script>
        </head>
        <body>
          <div id="react-view">${componentHTML}</div>
          <script type="application/javascript" src="/bundle.js"></script>
        </body>
      </html>
      `;

      return HTML;
    }

    co(function* (){
      yield fetchComponentData(store.dispatch, renderProps.components, renderProps.params)
    })
    this.body = renderView()
  });
});

var server = http.createServer(app.callback());
var io = socketio(server)

export {
    server, io
};
