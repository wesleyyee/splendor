import React, { Component, PropTypes } from 'react';
import { Navbar, NavItem, Nav } from 'react-bootstrap';
import { Link } from 'react-router';
import Login from './Login';
import Logout from './Logout';
import { loginUser, logoutUser } from '../actions/AuthActions'

if (process.env.BROWSER) {
  require('../../client/css/navbar.styl')
}

export default class MainMenu extends Component {

  render() {
    const { dispatch, auth } = this.props;
    return (
        <Navbar inverse>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">Splendit</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              {auth.isAuthenticated && !auth.pending &&
                <Logout onLogoutClick={() => dispatch(logoutUser())} />
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>
    )
  }

}

MainMenu.propTypes = {
  dispatch: PropTypes.func.isRequired,
  auth: PropTypes.any.isRequired
};