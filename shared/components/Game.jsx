import React, { Component , PropTypes }   from 'react';
import { bindActionCreators }             from 'redux';
import { connect }                        from 'react-redux';
import { checkAuth }                      from '../actions/AuthActions';
import { api }                            from '../actions/GameActions';
import { default as Board }               from './Board.jsx';
import { default as Card }                from './Card.jsx';
import { default as Player }              from './Player.jsx';

if (process.env.BROWSER) {
  require('../../client/css/game.styl')
}


class Game extends Component {
  static propTypes = {
    auth: PropTypes.any.isRequired,
    game: PropTypes.any.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.dispatch(api(`/api/game/${this.props.params.id}`, 'GET', null, 'game'))
  };

  _players(players) {
    var playersView = [];
    for (var i = 0; i < players.length; i++) {
      playersView.push(
          <Player
              name={players[i].name}
              time={players[i].time}
              reserved={players[i].reserved}
              tokens={players[i].tokens}
              cards={players[i].cards}
              nobles={players[i].nobles}
          />
      )
    }
    return playersView;
  };

  render() {
    const { dispatch, auth, game } = this.props

    console.log("game", this.props.game)
    if (game.hasOwnProperty("board")) {
      return (
          <div id="game" className="col-xs-12">
            <div className="col-xs-12 col-md-7">
              <Board board={game.board} gameID={this.props.params.id} {...this.props}/>
            </div>
            <div className="players col-xs-12 col-md-5">
              {this._players(game.players)}
            </div>
          </div>
      );
    } else {
      return false
    }
  }
}

export default connect(state => ({
  auth: state.auth,
  game: state.game
}))(Game)
