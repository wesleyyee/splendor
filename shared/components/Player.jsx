import React, { Component , PropTypes }   from 'react';
import { bindActionCreators }             from 'redux';
import { api }                            from '../actions/GameActions';
import { default as Card }                from './Card.jsx';
import { default as Tokens }              from './Tokens.jsx';

if (process.env.BROWSER) {
  require('../../client/css/player.styl')
}


class Player extends Component {
  static propTypes = {
    name: PropTypes.any.isRequired,
    time: PropTypes.any.isRequired,
    reserved: PropTypes.any.isRequired,
    tokens: PropTypes.any.isRequired,
    cards: PropTypes.any.isRequired
  };

  _reservedCards(cards) {
    var cardsView = []
    for (var i = 0; i < cards.length; i++) {
      cardsView.push(
          <div className="card-spacing">
            <Card
                disabled="true"
                tokens={cards[i].tokens}
                points={cards[i].points}
                discount_color={cards[i].discount_color}
                level={cards[i].level}
            />
          </div>)
    }
    return cardsView;
  };

  _boughtCards(cards) {
    var sortedCards = {
          white: [],
          blue: [],
          green: [],
          red: [],
          black: []
        },
        cardsView = [];
    for (var i = 0; i < cards.length; i++) {
      var card = cards[i]
      sortedCards[card.discount_color].push(
          <div className="owned-card">
            <span className="points">card.points</span>
            <span className={`discount-color ${card.discount_color}`}></span>
          </div>
      )
    }
    for (var color in sortedCards) {
      cardsView.push(<div className={`${color}-cards`}>{sortedCards[color]}</div>)
    }
    return (
        <div className="owned-cards">
          {cardsView}
        </div>
    )
  };

  render() {
    const { name, time, reserved, tokens, cards } = this.props;

    return (
        <div className={`player clearfix `}>
          <div className="header">
            <span className="name">{name}</span>
            <span className="time-left">{time}</span>
          </div>
          <div className="col-xs-12 col-sm-8">
            <div className="cards">{this._boughtCards(cards)}</div>
            <div className="owned-tokens">
              <Tokens disabled="true" tokens={tokens}/>
            </div>
          </div>
          <div className="reserved col-xs-12 col-sm-4">{this._reservedCards(reserved)}</div>
        </div>
    );
  }
}

export default Player
