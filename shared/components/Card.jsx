import React, { Component , PropTypes }   from 'react';
import { bindActionCreators }             from 'redux';
import { api }                            from '../actions/GameActions';
import { Modal }                          from 'react-bootstrap';

if (process.env.BROWSER) {
  require('../../client/css/card.styl')
}


class Card extends Component {
  static propTypes = {
    points: PropTypes.any.isRequired,
    tokens: PropTypes.any.isRequired,
    discount_color: PropTypes.any.isRequired,
  };

  state = {
    showModal: false
  };

  _buildTokenView(tokens) {
    var view = [];
    for (var color in tokens) {
      if (parseInt(tokens[color]) > 0) {
        view.push(
            <div className="token">
              <span className={`token-color ${color}`}></span>
              <span className="count">{`x${tokens[color]}`}</span>
            </div>
        )
      }
    }
    return view;
  };

  _cardModal() {
    return (
        <Modal
            show={this.state.showModal}
            onHide={()=>this._hideModal()}
            dialogClassName="card-modal"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg">Buy or Reserve</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <button className="btn" onClick={()=>
              this._submitAction(
                {"card_action": 1, "card_id": this.state.card}
            )}>
              BUY
            </button>
            <button className="btn" onClick={()=>
              this._submitAction({"card_action": 0, "card_id": this.state.card}
            )}>
              RESERVE
            </button>
          </Modal.Body>
        </Modal>
    )
  };

  _hideModal() {
    this.setState({
      showModal: false
    })
  };

  _onCardClick(cardId) {
    if (!this.props.hasOwnProperty("disabled")) {
      this.setState({
        card: cardId,
        showModal: true
      })
    }
  };

  _submitAction(action) {
    this.props.dispatch(api(`/api/game/${this.props.gameID}`, 'PUT', action, 'game'))
    this._hideModal()
  };

  render() {
    const { id, points, tokens, discount_color, level } = this.props;
    return (
      <div className={`card level${level}`} onClick={()=>this._onCardClick(id)}>
        <span className="points">{`-${points}-`}</span>
        <span className={`discount-color ${discount_color}`}></span>
        <div className="tokens">
          {this._buildTokenView(tokens)}
        </div>
        {this._cardModal()}
      </div>
    );
  }
}

export default Card
