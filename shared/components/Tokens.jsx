import React, { Component , PropTypes }        from 'react';
import { Modal }                               from 'react-bootstrap';
import { api }                                 from '../actions/GameActions'


class Tokens extends Component {

  static propTypes = {
    tokens: PropTypes.any.isRequired,
  };

  state = {
    selectedTokens: {
      "white": 0,
      "blue": 0,
      "green": 0,
      "red": 0,
      "black": 0
    },
    showModal: false
  }

  _tokenView(tokens, func, showGoldTokens) {
    var tokenList = [];

    for (var color in tokens) {
      if (parseInt(tokens[color]) > 0 && color != 'gold') {
        tokenList.push(
            <div className={`token ${color}`} onClick={func ? func.bind(this, color) : func}>
              <span className="count">{`x${tokens[color]}`}</span>
            </div>
        )
      }
    }
    if (showGoldTokens && tokens['gold'] > 0) {
      tokenList.push(
          <div className={`token gold`} onClick={func ? func.bind(this, 'gold') : func}>
            <span className="count">{`x${tokens['gold']}`}</span>
          </div>
      )
    }
    return tokenList
  };

  _tokenSelectionModal() {
    return (
      <Modal
        show={this.state.showModal}
        onHide={()=>this._hideModal()}
        dialogClassName="coin-modal">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">Select Tokens</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="token-selection">
            <div className="available-tokens">
              {this._tokenView(this.props.tokens, this._selectToken)}
            </div>
            <div className="selected-tokens">
              {this._tokenView(this.state.selectedTokens, this._returnToken)}
            </div>
          </div>
          <button onClick={()=>this._submitAction()}>OK</button>
        </Modal.Body>
      </Modal>
    )
  };

  _hideModal() {
    this.setState({
      showModal: false
    })
  };

  _selectToken(color) {
    console.log("COLOR", color)
    var currentSelectedTokens = this.state.selectedTokens,
        count = 0;

    for (var prop in currentSelectedTokens)
      count += currentSelectedTokens[prop]

    if (this.props.tokens[color] > 0 && count < 3 && this.state.selectedTokens[color] < 2) {
      currentSelectedTokens[color]++;
      this.props.tokens[color]--;
      this.setState({
        selectedTokens: currentSelectedTokens
      })
    }
  };

  _returnToken(color) {
    var currentSelectedTokens = this.state.selectedTokens;
    currentSelectedTokens[color]--;
    this.props.tokens[color]++;
    this.setState({
      selectedTokens: currentSelectedTokens
    })
  }

  _submitAction() {
    this.props.dispatch(api(`/api/game/${this.props.gameID}`, 'PUT', action, 'game'))
  };

  _onClick() {
    if (!this.props.hasOwnProperty("disabled"))
      this.setState({showModal: true})
  };

  render() {

    console.log("render")
    return (
      <div className="board-tokens">
        {this._tokenView(this.props.tokens, ()=>this._onClick(), true)}
        {this._tokenSelectionModal()}
      </div>
    );
  }
}

export default Tokens
