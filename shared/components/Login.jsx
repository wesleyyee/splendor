import React, { Component, PropTypes }               from 'react'
import { connect }                                   from 'react-redux';
import { Form, FormGroup, FormControl, Col, Button } from 'react-bootstrap'
import { loginUser }                                 from '../actions/AuthActions'

if (process.env.BROWSER) {
  require('../../client/css/login.styl')
}

class Login extends Component {

  render() {
    const { errorMessage } = this.props.auth
    console.log("error message", errorMessage)
    return (
        <div id="login">
          <form onSubmit={(event) => this.handleClick(event)} className="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4">
            <input type='text' ref='username' className="form-control" placeholder='Username'/>
            <input type='password' ref='password' className="form-control" placeholder='Password'/>
            <button type="submit" className="btn btn-primary">
              Login
            </button>
            {errorMessage &&
            <p className="error-message">{errorMessage.error}</p>
            }
          </form>
        </div>
    )
  }

  handleClick(event) {
    event.preventDefault()
    const { dispatch } = this.props
    const username = this.refs.username
    const password = this.refs.password
    const creds = { username: username.value.trim(), password: password.value.trim() }
    dispatch(loginUser(creds))
  }
}

export default connect(state => ({
  auth: state.auth
}))(Login)

Login.propTypes = {
  dispatch: PropTypes.func.isRequired
}