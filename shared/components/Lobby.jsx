import React, { Component , PropTypes }   from 'react';
import { bindActionCreators }             from 'redux';
import { checkAuth }                      from '../actions/AuthActions';
import { api, socketListener }            from '../actions/GameActions';
import { connect }                        from 'react-redux';
import { Link }                           from 'react-router'
import Navbar                             from './Navbar.jsx'

if (process.env.BROWSER) {
  require('../../client/css/board.styl')
}


class Home extends Component {
  static propTypes = {
    auth: PropTypes.any.isRequired,
    game: PropTypes.any.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.dispatch(api('/api/lobby', 'GET', null, 'lobby'))
    this.props.dispatch(socketListener('lobby'))
  };

  createGame() {
    this.props.dispatch(api('/api/lobby', 'POST', {}, 'game'))
  };

  startGame(uuid) {
    this.props.dispatch(api(`/api/game/${uuid}`, 'POST', {}, 'game'))
    this.props.history.pushState(null, `/game/${uuid}`)
  }

  render() {
    const { dispatch, auth, lobby } = this.props
    console.log("lobby", lobby);
    var games = [];
    for (var uuid in lobby) {
      games.push(
          <div>{uuid}<button className="btn btn-primary" onClick={()=>this.startGame(uuid)}>Join</button></div>
      )
    }
    return (
        <div id="lobby">
          <button className="btn btn-primary" onClick={()=>this.createGame()}>Create Game</button>
          <div className="games">
            {games}
          </div>
        </div>
    );
  }
}

export default connect(state => ({
  auth: state.auth,
  lobby: state.lobby
}))(Home)
