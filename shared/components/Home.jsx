import React, { Component , PropTypes }   from 'react';
import { bindActionCreators }             from 'redux';
import { checkAuth }                      from '../actions/AuthActions';
import { connect }                        from 'react-redux';
import Navbar                             from './Navbar.jsx';

if (process.env.BROWSER) {
  require('../../node_modules/bootstrap/dist/css/bootstrap.min.css')
  require('../../client/css/common.styl')
}


class Home extends Component {
  static propTypes = {
    auth: PropTypes.any.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentWillMount() {
    const { dispatch } = this.props
    if (typeof window != 'undefined') {
      dispatch(checkAuth())
    }
  };

  componentWillReceiveProps(nextProps) {
    var pathname = this.props.location.pathname
    if (!nextProps.auth.isAuthenticated && pathname.indexOf('login') === -1) {
      this.props.history.push('/login')
    }
    if (nextProps.auth.isAuthenticated != this.props.auth.isAuthenticated) {
      if (nextProps.auth.isAuthenticated && pathname.indexOf('login') > -1) {
        this.props.history.push('/')
      }
    }
  };

  render() {
    const { dispatch, auth } = this.props
    return (
      <div id="home">
        <Navbar auth={auth} dispatch={dispatch} history={this.props.history} />
        <div id="content">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  auth: state.auth
}))(Home)
