import React, { Component , PropTypes }   from 'react';
import { bindActionCreators }             from 'redux';
import { connect }                        from 'react-redux';
import { checkAuth }                      from '../actions/AuthActions';
import { api }                            from '../actions/GameActions';
import { default as Card }                from './Card.jsx';
import { default as Player }              from './Player.jsx';
import { default as Tokens }              from './Tokens.jsx';
import { Modal }                          from 'react-bootstrap';
import classNames                         from 'classnames';

if (process.env.BROWSER) {
  require('../../client/css/board.styl')
}


class Board extends Component {

  static propTypes = {
    board: PropTypes.any.isRequired,
  };

  state = {
    card: null,
    action: null,
    showModal: false
  }

  _fieldRow(cards) {
    var fieldView = [
      <div className="card-spacing">
        <div className={`card deck level${cards[0].level}`}>
          <div className="inner"></div>
        </div>
      </div>
    ];
    for (var i = 0; i < cards.length; i++) {
      fieldView.push(
        <div className="card-spacing">
          <Card
              onClick={(id)=>this._onCardClick(id)}
              id={cards[i].id}
              tokens={cards[i].tokens}
              points={cards[i].points}
              discount_color={cards[i].discount_color}
              level={cards[i].level}
              {...this.props}
          />
        </div>)
    }
    return fieldView;
  };

  render() {
    var board = this.props.board;
    return (
      <div id="board" className={classNames({
          'show-coin-modal': this.state.showCoinModal,
          'show-card-modal': this.state.showCardModal})}>
        <div className="nobles"></div>
        <div className="field">
          <div className="level3 clearfix">{this._fieldRow(board[3])}</div>
          <div className="level2 clearfix">{this._fieldRow(board[2])}</div>
          <div className="level1 clearfix">{this._fieldRow(board[1])}</div>
          <Tokens tokens={board.tokens} {...this.props}/>
        </div>
      </div>
    );
  }
}

export default Board
