import React                            from 'react';
import { Route, IndexRoute, NoMatch }   from 'react-router';
import App                              from 'components/index';
import Home                             from 'components/Home';
import Login                            from 'components/Login';
import Game                             from 'components/Game';
import Lobby                            from 'components/Lobby';

export default (
  <Route name="app" component={App}>
    <Route component={Home} path="/">
      <IndexRoute component={Lobby}/>
      <Route path="/game/:id" component={Game}/>
      <Route path="/login" component={Login}/>
      <Route path="*" component={NoMatch}/>
    </Route>
  </Route>
);
