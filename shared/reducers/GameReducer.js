import { combineReducers } from 'redux'
import {
    API_FAILURE, API_REQUEST, API_SUCCESS,
    SOCKET_FAILURE, SOCKET_SUCCESS, SOCKET_MSG
} from '../actions/GameActions'

// The auth reducer. The starting state sets authentication
// based on a token being in local storage. In a real app,
// we would also want a util to check if the token is expired.
function game(state = {}, action) {
  switch (action.type) {
    case API_REQUEST:
      return Object.assign({}, state, {})
    case API_SUCCESS:
      if (action.reducer === 'game') {
        console.log("game reducer", action.response)
        return Object.assign({}, state, action.response)
      } else {
        return Object.assign({}, state, {})
      }
    case API_FAILURE:
      return Object.assign({}, state, {})
    case SOCKET_MSG:
      console.log("SOCKET MSG", action)
      return Object.assign({}, state, action.message)
    case SOCKET_SUCCESS:
      return Object.assign({}, state, {})
    case SOCKET_FAILURE:
      return Object.assign({}, state, {})
    default:
      return state
  }
}

export default game