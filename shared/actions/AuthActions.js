import request from 'axios'
import { Router } from 'react-router'

export const CHECK_STATUS = 'CHECK_STATUS'
// There are three possible states for our login
// process and we need actions for each of them
export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
// Three possible states for our logout process as well.
// Since we are using JWTs, we just need to remove the token
// from localStorage. These actions are more useful if we
// were calling the API to log the user out
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST'
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE'


export function checkAuth() {
  if (localStorage.getItem('id_token')) {
    let config = {
      method: 'POST',
      headers: {
        'Content-Type':'application/json',
        'Authorization': localStorage.getItem('id_token')
      }
    }
    return dispatch => {
      return request('/api/validate', config)
        .then((res, body) =>  {
          console.log("validate res", res)
          if (res.status === 200) {
            // Dispatch the success action
            dispatch(receiveLogin({}))
          }
        }).catch(res => {
          dispatch(receiveLogout())
        })
    }
  } else {
    return receiveLogout()
  }
}

function requestStatus() {
  return {
    type: CHECK_STATUS,
    isAuthenticated: false,
    pending: false
  }
}

function requestLogin(creds) {
  return {
    type: LOGIN_REQUEST,
    isAuthenticated: false,
    pending: true,
    creds
  }
}

function receiveLogin(user) {
  return {
    type: LOGIN_SUCCESS,
    isAuthenticated: true,
    id_token: user.id_token,
    pending: false
  }
}

export function loginUser(creds) {
  let config = {
    method: 'POST',
    headers: { 'Content-Type':'application/json' },
    data: {
      username: creds.username,
      password: creds.password
    }
  }

  return dispatch => {
    // We dispatch requestLogin to kickoff the call to the API
    dispatch(requestLogin(creds))

    return request('/api/login', config)
        .then((res, body) =>  {
          if (res.status === 200) {
            console.log("res", res)
            // If login was successful, set the token in local storage
            localStorage.setItem('id_token', res.data.access_token)
            // Dispatch the success action
            dispatch(receiveLogin({}))
          } else {
            dispatch(receiveLogout())
          }
        }).catch(res => dispatch(loginError({error: res.statusText})))
  }
}

function loginError(message) {
  return {
    type: LOGIN_FAILURE,
    isAuthenticated: false,
    pending: false,
    message
  }
}

function requestLogout() {
  return {
    type: LOGOUT_REQUEST,
    isAuthenticated: true,
    pending: false
  }
}

export function receiveLogout() {
  return {
    type: LOGOUT_SUCCESS,
    isAuthenticated: false,
    pending: false
  }
}

// Logs the user out
export function logoutUser() {
  return dispatch => {
    dispatch(requestLogout())
    localStorage.removeItem('id_token')
    dispatch(receiveLogout())
  }
}