import { CALL_API }        from '../middleware/api'
import { SOCKET_LISTENER } from '../middleware/socket'

export const API_REQUEST = 'API_REQUEST'
export const API_SUCCESS = 'API_SUCCESS'
export const API_FAILURE = 'API_FAILURE'

export const SOCKET_SUCCESS = 'SOCKET_SUCCESS'
export const SOCKET_FAILURE = 'SOCKET_FAILURE'
export const SOCKET_SUBSCRIBE = 'SOCKET_SUBSCRIBE'
export const SOCKET_MSG = 'SOCKET_MSG'

// Uses the API middlware to call Bankchain
export function api(path, method, data, reducer) {
  return {
    [CALL_API]: {
      endpoint: path,
      method: method,
      data: data,
      reducer: reducer,
      authenticated: true,
      types: [API_REQUEST, API_SUCCESS, API_FAILURE]
    },
    type: API_REQUEST
  }
}

export function socketListener(channel) {
  return {
    [SOCKET_LISTENER]: {
      channel: channel,
      types: [SOCKET_SUCCESS, SOCKET_FAILURE]
    },
    type: SOCKET_SUBSCRIBE
  }
}

export function socketMessage(msg) {
  return {
    type: SOCKET_MSG,
    isAuthenticated: true,
    pending: false,
    message: msg
  }
}