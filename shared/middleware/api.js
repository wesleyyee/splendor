import request        from 'axios'
import config         from '../../config'
import { logoutUser, receiveLogout } from '../actions/AuthActions'


/**
 * All calls to Bankchain and Core POST to the '/auth' endpoint of the web app.
 * The body contains an options object which are the specifics of the respective call.
 */
function callApi(endpoint, method, data, authenticated, dispatch) {
  let token = localStorage.getItem('id_token') || null;
  let options = {
    url: endpoint,
    method: method,
    headers: {
      'Content-Type':'application/json'
    }
  };

  if(authenticated) {
    if(token) {
      options.headers.Authorization = `${token}`
    } else {
      dispatch(receiveLogout())
    }
  }

  if (method != 'GET' && data) {
    options.data = JSON.stringify(data)
  }
  console.log("options", options)
  return request(options)
      .then((res, body) =>  {
        console.log("RES", res)
        if (res.status != 200) {
          return Promise.reject(res.body)
        }
        return res.data
      }).catch((err) => {
        if (err.status === 401) {
          dispatch(logoutUser())
        }
      })
}

export const CALL_API = Symbol('Call API')

export default store => next => action => {
  const actionDetails = action[CALL_API]

  // So the middleware doesn't get applied to every single action
  if (typeof actionDetails === 'undefined') {
    return next(action)
  }

  let { endpoint, method, data, reducer, authenticated, types } = actionDetails;

  const [ requestType, successType, errorType ] = types;

  return callApi(endpoint, method, data, authenticated, store.dispatch).then(
      response =>
          next({
            response,
            reducer,
            authenticated,
            type: successType,
          }),
      error => next({
        error: error.message || 'There was an error.',
        type: errorType
      })
  )
}