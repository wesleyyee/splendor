import io from 'socket.io-client'
import { socketMessage } from '../actions/GameActions'

if (!process.env.hasOwnProperty("NODE")) {
  var socket = io.connect();
}
export const SOCKET_LISTENER = Symbol('Socket Listener')

export default store => next => action => {
  console.log("socket middleware")
  const listener = action[SOCKET_LISTENER]

  // So the middleware doesn't get applied to every single action
  if (typeof listener === 'undefined') {
    return next(action)
  }

  let { channel, types } = listener
  console.log("types", types)
  console.log("subscribe to channel", channel)
  const [ successType, errorType ] = types
  console.log("initial connect? ", socket.connected)
  socket.on(channel, function (res) {
    console.log("socket res", res)
    store.dispatch(socketMessage(res))
  }).on('connect', function() {
    console.log("socket connected")
  })
  return next({
    type: successType
  })
}