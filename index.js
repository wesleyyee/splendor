'use strict';

require('babel-core/register')({});
require('babel-polyfill');

var server = require('./server').server;

const PORT = process.env.PORT || 3001;

server.listen(PORT);
console.log('Server listening on: ' + PORT);