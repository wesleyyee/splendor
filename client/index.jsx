import React                 from 'react';
import { render }            from 'react-dom';
import { Router }            from 'react-router';
import createBrowserHistory  from 'history/lib/createBrowserHistory'
import { Provider }          from 'react-redux';
import { auth, game, lobby } from 'reducers';
import routes                from 'routes';
import thunkMiddleware       from 'redux-thunk'
import { default as api }    from '../shared/middleware/api'
import { default as socket } from '../shared/middleware/socket'
import immutifyState         from 'lib/immutifyState';
import { createStore,
         combineReducers,
         applyMiddleware }  from 'redux';

const initialState = window.__INITIAL_STATE__;
const reducer = combineReducers({
  auth,
  game,
  lobby
});
const history = createBrowserHistory();
const store   = applyMiddleware(thunkMiddleware, api, socket)(createStore)(reducer, initialState);

render(
  <Provider store={store}>
    <Router children={routes} history={history} />
  </Provider>,
  document.getElementById('react-view')
);
