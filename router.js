import koaRouter        from 'koa-router';
import jwt              from 'jsonwebtoken';
import bcrypt           from 'bcrypt';
import redis            from 'redis';
import serialize        from 'node-serialize';
import { config }       from './config';
import knexfile         from './knexfile.js'
import knexLib          from 'knex'
import { io }           from './server.jsx'
import PythonShell      from 'python-shell'

var knex = knexLib(knexfile.development),
    router = koaRouter(),
    redisClient = redis.createClient({
      host: config.redis.host,
      port: config.redis.port
    });

/**
 * USER AUTH ENDPOINTS
 */

function hashPassword(password) {
  return function(cb) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(password, salt, function(err, hash) {
        cb(null, hash)
      });
    });
  }
}

function checkPassword(password, hash) {
  return function(cb) {
    bcrypt.compare(password, hash, function(err, res) {
      cb(null, res)
    });
  }
}

router.post('/api/login', function* (){
  try {
    var username = this.request.body.username,
        password = this.request.body.password,
        db_user = yield knex('users').where({username: username}).select();
    if (db_user.length > 0) {
      // Login user
      var valid = yield checkPassword(password, db_user[0].password)
      if (!valid) {
        this.status = 401;
        this.body = {"error": "Invalid credentials"}
        return
      }
    } else {
      // Create user
      var hash_pw = yield hashPassword(password);
      yield knex('users').insert({username: username, password: hash_pw})
    }
    this.status = 200;
    this.body = {access_token: jwt.sign({username: username}, config.secret)}
  } catch(e) {
    this.throw(e, 500)
  }
});

router.post('/api/validate', function* () {
  try {
    jwt.verify(this.req.headers.authorization, config.secret);
    this.status = 200;
    this.body = {"success": true}
  } catch(e) {
    this.throw(e, 401)
  }
});


/**
 * GAME ENDPOINTS
 */
var GAMES = {};

router.get('/api/lobby', function* () {
  try {
    var lobby = {}
    for (var uuid in GAMES) {
      lobby[uuid] = GAMES[uuid].lobby
    }
    this.body = lobby
    this.status = 200;
  } catch(e) {
    this.throw(e, 500);
  }
});

router.post('/api/lobby', function* () {
  try {
    var uuid = Math.random()*(10000000000000000000);
    GAMES[uuid] = {}
    GAMES[uuid]["lobby"] = [];
    var socketRes = {}
    socketRes[uuid] = GAMES[uuid]["lobby"]
    io.emit('lobby',socketRes)
    this.body = {uuid: uuid}
    this.status = 200;
  } catch (e) {
    this.throw(e, 500);
  }
});

router.post('/api/lobby/:id', function* () {

});

router.get('/api/game/:id', function* () {
  try {
    var shell = GAMES[this.params.id]["shell"]
    var msg = {}
    if (shell) {
      msg = yield function(cb) {
        shell.send('status')
        shell.on('message', function(data) {
          cb(null, data.substring(11))
        })
      }
    }
    this.body = msg
    this.status = 200
  } catch(e) {
    this.throw(e, 500)
  }
});

router.post('/api/game/:id', function* () {
  try {
    var uuid = this.params.id,
        engine = yield function(cb) {
          GAMES[uuid]["shell"] = new PythonShell('splendor.py');
          var shell = GAMES[uuid]["shell"]
          shell.send('status')
          shell.on('message', function (data) {
            cb(null, data.substring(11))
          })
        };

    this.body = engine;
    this.status = 200;
  } catch(e) {
    this.throw(e, 500)
  }
});

router.put('/api/game/:id', function* () {
  try {
    var self = this
    var shell = GAMES[this.params.id]["shell"];
    var engine = yield function(cb) {
      console.log("game move", JSON.stringify(this.request.body))
      shell.send(JSON.stringify(this.request.body))
      shell.on('message', function(data) {
        console.log("shell", data)
        cb(null, data);
      })
    }
    if (engine.substring(0, 5) === "ERROR") {
      this.status = 500;
    } else {
      this.status = 200;
    }
    this.body = engine.substring(11);
  } catch(e) {
    this.throw(e, 500)
  }
})

router.delete('/api/game/:id', function* () {
  try {
    var self = this;
    yield function(cb) {
      GAMES[this.params.id].end(function(err) {
        if (err) {
          this.throw(err, 500)
        }
        self.status = 403 // TODO: change to deleted status code
        cb()
      })
    };
  } catch(e) {
    this.throw(e, 500)
  }
});

export default router