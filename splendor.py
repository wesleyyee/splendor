"""splendor game"""
import random
import uuid
import csv
from collections import defaultdict
import time
import logging
import sys
import json
import simplejson


NOT_TURN, TURN, TOO_MANY_TOKENS, RETURN_TOKENS, TURN_DONE = range(5)
RESERVE, BUY, NONE = range(3)
CARD_COLORS = ["white", "blue", "green", "red", "black"]


class TooManyTokens(Exception):
    """exception for too many tokens"""
    pass


class NotEnoughMoney(Exception):
    """exception for not enough money"""
    pass


class GameOver(Exception):
    """exception indicating end of game"""
    pass


class Noble(object):
    """class to store noble tiles"""

    def __init__(self, cost):
        """cost is a dict of cost color to number"""
        #self.log = logging.getLogger("Noble")
        #self.log.debug("Noble tile init")
        self.cost = cost
        self.points = 3

    def get_points(self):
        """return number of points for noble tile"""
        #self.log.debug("Noble get points")
        return self.points

    def check_cost(self, card_list):
        """check if cost of noble tile has been satisfied"""
        #self.log.debug("Noble check cost")
        card_list_value = defaultdict(int)
        for card in card_list:
            card_list_value[card.get_discount()] += 1
        for key in self.cost:
            if card_list_value[key] < self.cost[key]:
                return False
        return True


class Card(object):
    """class to store card info"""
    _discount_mapping = {
        "w": "white",
        "u": "blue",
        "g": "green",
        "r": "red",
        "k": "black",
    }

    def __init__(self, discount, tokens, level):
        #self.log = logging.getLogger("Card")
        #self.log.debug("Card init")
        self.points = int(discount[1])
        self.discount = discount[0]
        self.discount_color = self._discount_mapping[self.discount]
        self.tokens = tokens
        self._id = uuid.uuid4().get_hex()
        self.level = level

    def as_json(self):
        return {
            "id": self._id,
            "level": self.level,
            "points": self.points,
            "discount_color": self.discount_color,
            "tokens": dict(zip(CARD_COLORS, self.tokens))
        }

    def __hash__(self):
        return hash(self._id)

    def get_level(self):
        """get the level of the card"""
        #self.log.debug("Card get level %d", self.level)
        return self.level

    def get_price(self, current_player_discount):
        """returns the current price tuple taking into account the current player discount"""
        discounted_price = []
        for i, color in enumerate(CARD_COLORS):
            #self.log.debug(self.tokens[i])
            #self.log.debug(current_player_discount[color])
            discounted_price.append(max([0, int(self.tokens[i]) - int(current_player_discount[color])]))
        return tuple(discounted_price)

    def get_discount(self):
        """get discount from card"""
        #self.log.debug("Card get discount")
        return self.discount

    def get_points(self):
        """get points from card"""
        #self.log.debug("Card get points %d", self.points)
        return self.points

    def get_id(self):
        """get the card id"""
        return self._id

    def __repr__(self):
        return "<Card %d, %d, %s, %s>" % (self.level, self.points, self.discount, self.tokens)


class Player(object):
    """class to store player info"""

    def __init__(self, name, timelimit):
        #self.log = logging.getLogger("Player")
        #self.log.debug("Player init")
        self.first_player = False
        self.player_state = NOT_TURN
        self.name = name
        self.tokens = defaultdict(int)
        self.cards = []
        self.reserved = []
        self.nobles = []
        self._id = uuid.uuid4().get_hex()
        self.time_left = timelimit

    def as_json(self):
        return {
            "name": self.name,
            "tokens": self.tokens,
            "cards": [card.as_json() for card in self.cards],
            "reserved": [card.as_json() for card in self.reserved],
            "nobles": self.nobles,
            "time_left": self.time_left
        }

    def add_tokens(self, token_list):
        """method to add tokens to a player"""
        #self.log.debug("Player add tokens")
        for token in token_list:
            self.tokens[token] += 1
        if self.total_tokens() > 10:
            self.player_state = TOO_MANY_TOKENS
            raise TooManyTokens

    def set_first_player(self):
        """set player as first player"""
        self.first_player = True
        #self.log.debug("Set first player True")

    def get_total_points(self):
        """get total points for player"""
        #self.log.debug("Player get total points")
        points = 0
        for card in self.cards:
            points += card.get_points()
        for noble in self.nobles:
            points += noble.get_points()
        return points

    def get_reserve_card_num(self):
        """get number of reserved cards for the player"""
        #self.log.debug("Player get reserve card num")
        return len(self.reserved)

    def total_tokens(self):
        """method to return total number of tokens a player has"""
        #self.log.debug("get the total number of tokens for the player")
        return sum(v for v in self.tokens.values())

    def get_total_discount(self):
        """returns the current discount for the player based on cards owned"""
        discount = defaultdict(int)
        for c in self.cards:
            discount[c.discount_color] += 1
        return discount

    def spend_tokens(self, color, number):
        """spend color tokens first, spend gold if not enough
        return list of tokens that were spent"""
        tokens_spent = []
        if self.tokens[color] < number:
            if self.tokens["gold"] + self.tokens[color] < number:
                raise NotEnoughMoney
            tokens_spent += [color]*self.tokens[color]
            tokens_spent += ["gold"]* (number - self.tokens[color])
            self.tokens["gold"] -= (number - self.tokens[color])
            self.tokens[color] = 0
        else:
            tokens_spent += [color]*number
            self.tokens[color] -= number
        return tokens_spent

    def buy_card(self, card):
        """buys card, returns tokens used"""
        current_discount = self.get_total_discount()
        discounted_card_price = card.get_price(current_discount)
        tokens_to_spend = []
        for i, color in enumerate(CARD_COLORS):
            tokens_to_spend += self.spend_tokens(color, discounted_card_price[i])
        return tokens_to_spend

    def take_turn(self, card, token_list, card_action):
        """take a turn for a player"""
        #self.log.debug("Player take turn")
        #self.log.debug(card is not None)
        self.player_state = TURN_DONE
        if token_list is not None:
            try:
                #self.log.debug("Add Tokens")
                self.add_tokens(token_list)
            except TooManyTokens:
                #self.log.warning("too many tokens")
                self.player_state = TOO_MANY_TOKENS
        if card_action == RESERVE and card is not None:
            #self.log.debug("Reserve")
            self.reserved.append(card)
        elif card_action == BUY and card is not None:
            #self.log.debug("Buy")
            tokens_to_return = self.buy_card(card)
            return tokens_to_return
        #self.log.debug("RETURN FROM PLAYER")
        return []

    def add_noble(self, noble):
        """adds a noble to the player"""
        self.nobles.append(noble)

    def check_noble(self, noble):
        """checks if player should get a noble"""
        if noble.check_cost(self.cards):
            return True
        else:
            return False

    def __repr__(self):
        return "<Player %s: tokens[%s] cards[%s] reserved[%s] time[%d] first[%s]>" % (
            self.name, self.tokens, self.cards, self.reserved, self.time_left, self.first_player)


class Game(object):
    """class to store game state"""

    def __init__(self, numplayers, timelimit):
        #self.log = logging.getLogger("Game")
        #self.log.debug("Game init")
        self.players = [Player(str(n), timelimit) for n in xrange(numplayers)]
        self.tokens = {}
        self.nobles = []
        self.time_keeper = int(time.time())
        for color in CARD_COLORS:
            self.tokens[color] = 7
        self.tokens["gold"] = 5
        self.cards = {
            1: [],
            2: [],
            3: [],
        }
        self.board_cards = {
            1: [],
            2: [],
            3: [],
        }
        with open('./cards.csv', 'r') as csv_file:
            cardreader = csv.reader(csv_file, delimiter=',')
            for row in cardreader:
                self.cards[int(row[0])].append(Card((row[1], row[2]), tuple(row[3:]), int(row[0])))

    def as_json(self):
        return {
          "board": {
              1: [card.as_json() for card in self.board_cards[1]],
              2: [card.as_json() for card in self.board_cards[2]],
              3: [card.as_json() for card in self.board_cards[3]],
              "tokens": self.tokens
          },
          "players": [player.as_json() for player in self.players]
        }

    def shuffle(self):
        """shuffle the cards in the deck"""
        #self.log.debug("shuffle the deck")
        for key in self.cards:
            random.shuffle(self.cards[key])

    def deal_card(self, level):
        """deal one card"""
        #self.log.debug("deal a card, level=%d", level)
        card = self.cards[level].pop()
        self.board_cards[level].append(card)

    def deal_cards_to_start(self):
        """deal 12 cards to start the game"""
        #self.log.debug("deal cards to start the game")
        for _ in xrange(4):
            for i in xrange(1, 4):
                self.deal_card(i)

    def find_card_by_id(self, card_id):
        """finds card on board by id and removes it"""
        #self.log.debug("find a card by the card id")
        for card in self.board_cards[1] + self.board_cards[2] + self.board_cards[3]:
            if card.get_id() == card_id:
                return card

    def take_card_from_board(self, card):
        """take a card from the board"""
        #self.log.debug("take a card from the board")
        if card in self.board_cards[card.get_level()]:
            self.board_cards[card.get_level()].remove(card)
            self.deal_card(card.get_level())
            return
        if card == self.cards[card.get_level()][-1]:
            self.cards[card.get_level()].pop()
            return
        raise Exception("Something went wrong")


    def get_card_from_deck(self, deck_level):
        """remove card from deck and return"""
        #self.log.debug("get a card from the deck (leave it there)")
        if len(self.cards[deck_level]) > 0:
            return self.cards[deck_level][-1]
        else:
            raise Exception("No cards left in deck")

    def start_game(self, points_to_win=15):
        """starts game by dealing cards"""
        #self.log.debug("start the game, points to win: %d", points_to_win)
        self.points_to_win = points_to_win
        self.shuffle()
        self.deal_cards_to_start()
        random.shuffle(self.players)
        self.players[0].set_first_player()
        self.time_keeper = int(time.time())
        #self.log.debug(self.tokens)
        #self.log.debug(self.board_cards)
        #self.log.debug(self.players)

    def take_tokens(self, token_list):
        """remove tokens from board for player"""
        #self.log.debug("take tokens from the board")
        tokens_taken = []
        error = None
        try:
            if len(token_list) > 3:
                raise Exception("Can only take 3 tokens")
            if len(token_list) > 2 and len(set(token_list)) == 1:
                raise Exception("Can only take two of same token")
            if len(token_list) == 2 and len(set(token_list)) == 1:
                if self.tokens[token_list[0]] < 4:
                    raise Exception("Cannot take two tokens when less than 4")
            for token in token_list:
                if self.tokens[token] == 0:
                    raise Exception("Cannot take token that doesn't exist")
                self.tokens[token] -= 1
                tokens_taken.append(token)
        except Exception as e:
            error = str(e)
        return tokens_taken, error

    def get_gold_tokens(self):
        """get the number of gold tokens available"""
        #self.log.debug("get number of gold tokens remaining")
        return self.tokens["gold"]

    def add_tokens(self, token_list):
        """add tokens back to board"""
        #self.log.debug("add tokens back to the board")
        for token in token_list:
            self.tokens[token] += 1

    def check_nobles(self, player):
        """check if a player should get a noble tile and add the noble if they should"""
        noble_to_add = None
        for noble in self.nobles:
            if player.check_noble(noble):
                noble_to_add = noble
                break
        if noble_to_add is not None:
            player.add_noble(noble_to_add)
            self.nobles.remove(noble_to_add)

    def check_game_end(self):
        """checks if any player has more than points_to_win points"""
        #self.log.debug("check if the game is over")
        return any(p.get_total_points() >= self.points_to_win for p in self.players)

    def next_turn(self, card_id=None, token_list=None, card_action=NONE, deck_level=0):
        """initiate the next turn
        card_id: card id to take from board
        token_list: list of tokens to choose
        card_action: action for a card: NONE, BUY, RESERVE
        deck_level: choose card from deck_level (0 default)

        returns player to go next (if the player needs more action for the turn,
        the same player that just went may need to go again)
        """
        #self.log.debug("perform next turn")
        if self.players[0].first_player:
            #self.log.debug("end of round, check for end of game")
            if self.check_game_end():
                #self.log.debug("GAME END")
                raise GameOver("Game Over")
        player = self.players.pop(0)
        args = (card_id, token_list, card_action, deck_level)
        if player.player_state == NOT_TURN:
            #self.log.debug("player NOT_TURN, starting TURN")
            player.player_state = TURN
            try:
                self.normal_turn(player, args)
            except Exception as e:
                self.players = [player] + self.players
                raise Exception(e)
        elif player.player_state == TOO_MANY_TOKENS:
            #self.log.debug("player TOO_MANY_TOKENS, starting RETURN_TOKENS")
            player.player_state = RETURN_TOKENS
            self.return_tokens_turn(player, args)
        if player.player_state == TURN_DONE:
            #self.log.debug("player TURN_DONE, rotating players")
            self.check_nobles(player)
            player.player_state = NOT_TURN
            self.players = self.players + [player]
        elif player.player_state == TOO_MANY_TOKENS:
            #self.log.debug("player TOO_MANY_TOKENS, not rotating players")
            # put player back at beginning
            self.players = [player] + self.players
        #self.log.debug("next player: %s", self.players[0]._id)
        return self.players[0]._id

    def return_tokens_turn(self, player=None, args=None):
        """logic for turn where player took too many tokens and needs
        to return tokens"""
        #self.log.debug("start return tokens turn")
        assert player is not None
        assert args is not None
        card_id, token_list, card_action, deck_level = args
        if card_id is not None:
            raise Exception("Invalid Turn")
        if deck_level > 0:
            raise Exception("Invalid Turn")
        if card_action != NONE:
            raise Exception("Invalid Turn")
        player.remove_tokens(token_list)
        self.add_tokens(token_list)
        player.player_state = TURN_DONE

    def normal_turn(self, player=None, args=None):
        """logic for normal beginning of turn"""
        #self.log.debug("start normal turn")
        assert player is not None
        assert args is not None
        card_id, token_list, card_action, deck_level = args
        if card_id is not None and deck_level > 0:
            raise Exception("Invalid Turn, Choose Card or Deck")
        if card_id is not None and token_list is not None:
            raise Exception("Invalid Turn, Choose Card or Tokens")
        if token_list is not None and deck_level > 0:
            raise Exception("Invalid Turn, Choose Deck or Tokens")
        if deck_level > 0 and card_action != RESERVE:
            raise Exception("Invalid Turn, Can only reserve cards from Deck")
        if card_id is not None or deck_level > 0 and card_action == RESERVE:
            if player.get_reserve_card_num() == 3:
                raise Exception("Invalid Turn, Too Many reserved Cards")
        if card_id is not None:
            card = self.find_card_by_id(card_id)
        elif deck_level > 0:
            card = self.get_card_from_deck(deck_level)
        else:
            card = None
        if card is not None and card_action == RESERVE:
            if self.get_gold_tokens() > 0:
                token_list = ["gold"]
        if token_list is not None:
            token_list, error = self.take_tokens(token_list)
            if error is not None:
                raise Exception("Invalid Turn, tokens")
        valid_turn = True
        try:
            tokens_to_return = player.take_turn(card, token_list, card_action)
        except NotEnoughMoney:
            valid_turn = False
            raise Exception("Invalid Turn")
        if valid_turn:
            if card is not None:
                self.take_card_from_board(card)
            self.add_tokens(tokens_to_return)

    def which_player_turn(self):
        """return the id of the player that should be playing next"""
        return self.players[0]._id


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    log = logging.getLogger("MAIN")
    G = Game(4, 5 * 60)
    G.start_game()
    while True:
        try:
            args = raw_input()
            if args == 'status':
                log.debug(json.dumps(G.as_json()))
            else:
                newargs = json.loads(args)
                if 'card_action' in newargs:
                    newargs['card_action'] = int(newargs['card_action'])
                G.next_turn(**newargs)
                log.debug(json.dumps(G.as_json()))
        except Exception as e:
            log.error(e)
            pass
